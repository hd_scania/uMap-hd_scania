![795X](./galleries/displays/795X.png) Citybus 795X
[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)
# The OpenStreetMap-based open sourcing bus route projects for Hong Kong by HD Scania
1. https://uMap.OSM.ch/en/user/hd_scania (main public preview project now)
2. https://uMap.OpenStreetMap.fr/en/user/hd_scania (first public preview project ever)
3. https://uMap.OpenStreetMap.de/en/user/hd_scania (KMB/LWB only previews project)
4. https://marble.kde.org
## Objectives to introduce the projects
We Hongkongers are always suffering for the SARegime’s giving ways to ‘‘The Tracks’’ (The Railways), but in fact the tracks are always caught much less flexible than the on road public transports like the buses and the light coaches, it’s everything by their own nature!

And the Tracks have also been caught for being overloaded for much decades since the MTR mergers of KCR at 2007, and we really have to strengthen and enhance those bus services since never ever before! We couldn’t be solely reliant on the Tracks anymore, buses now have to take the Top Role for the public transportations in Hong Kong soon!

So I’m here to make bus routings much more reasonable ever for Hong Kong, and my below galleries are demonstrating my working and product mapping schemes for the buses.
## Mirroring
1. [`https://git.code.sf.net/p/umap-hdscania/code`](https://sf.net/p/umap-hdscania/code) (Primary project)
2. https://notabug.org/HD_Scanius/uMap-hd_scania (Not-a-bug mirror)
3. https://gitlab.com/hd_scania/uMap-hd_scania (GitLab mirror)
## Galleries
### uMap, OpenStreetMap products
![NTE-E_IEC](./galleries/042_222/NTE.8E.uMap.png) Tsing-sha highway bus routes group towards Eastern Hong Kong island
![KMB 900,935,936](./galleries/042_222/900.935.936.KMB.png) Citybus (blue) 303,307,930,983,170, KMB (red) 900,935,936, and NWFB (orange) 948 etc groups of bus routes
### KDE Marble, OpenStreetMap wokring desktops
![795](./galleries/012_623/795.012_523.png) Citybus 795 (daytimes only)
![795X](./galleries/012_623/795X.012_623.png) Citybus 795X
![N796](./galleries/010_923/N796.121_522.png) Citybus N796 (overnight only)
![296m](./galleries/012_623/296m.012_223.png) KMB 296m
![98 (KMB)](./galleries/012_623/98.KMB.012_523.png) KMB 98
![98b](./galleries/012_623/98b.012_523.png) KMB 98b
![296d](./galleries/010_923/296d.010_923.png) KMB 296d
![296a](./galleries/010_923/296a.010_823.png) KMB 296a
![98a](./galleries/010_923/98a.010_823.png) KMB 98a
![298](./galleries/010_923/298.010_823.png) KMB 298
![170](./galleries/042_222/170.png) Citybus 170/N170 specifically strengthened for Sham-shui-po, Mongkok, and Jordan
